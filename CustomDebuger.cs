using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomDebuger : MonoBehaviour
{
    public string SomeStringValue;

    private void Start()
    {
        IDebugable debugable = new DebugDecorator();
        DebugDecoratorA debugDecoratorA = new DebugDecoratorA(debugable);
        DebugDecoratorB debugDecoratorB = new DebugDecoratorB(debugDecoratorA);

        debugDecoratorA.Show(SomeStringValue);
        debugDecoratorB.Show(SomeStringValue);
    }
}
