using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DebugDecoratorB : CustomDebugerDecorator
{
    public DebugDecoratorB(IDebugable debugable) : base(debugable){}

    public void Show(string s)
    {
        base.Show(s);
        File.WriteAllText(@"", s);
    }
}
