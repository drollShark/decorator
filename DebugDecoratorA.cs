using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDecoratorA : CustomDebugerDecorator
{
    public DebugDecoratorA(IDebugable debugable) : base(debugable){}

    public void Show(string s)
    {
        base.Show(s);
        Debug.Log(s.Length);
    }
}
