using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomDebugerDecorator : IDebugable
{
    private IDebugable debugable;


    public CustomDebugerDecorator(IDebugable debugable)
    {
        this.debugable = debugable;
    }

    public void Show(string s)
    {
        if(this.debugable != null)
        {
            this.debugable.Show(s);
        }
    }


}
